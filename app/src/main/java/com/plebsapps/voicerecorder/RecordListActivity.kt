package com.plebsapps.voicerecorder

import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import com.plebsapps.voicerecorder.R.layout.record_list
import java.io.File
import java.io.IOException
import java.util.*
import kotlinx.android.synthetic.main.item_recording.view.*
import kotlinx.android.synthetic.main.record_list.*

@RequiresApi(Build.VERSION_CODES.KITKAT)
class RecordListActivity : AppCompatActivity(), RecordAdapter.OnClickListener {
    private val mHandler = Handler()

    private var lastProgress = 0
    private var isPlaying = false
    private var lastIndex = -1
    private var recordAdapter: RecordAdapter? = null
    private var mediaPlayer: MediaPlayer? = null

    override fun onClickPlay(view: View, record: Recording, recordingList: ArrayList<Recording>, position: Int) {
        playRecords(view, record, recordingList, position)
    }

    private fun playRecords(view: View, recordItem: Recording, recordingList: ArrayList<Recording>, position: Int) {
        val recording = recordingList[position]

        if (isPlaying) {
            stopPlaying()
            if (position == lastIndex) {
                recording.isPlaying = false
                stopPlaying()
                recordAdapter!!.notifyItemChanged(position)
            } else {
                markAllPaused(recordingList)
                recording.isPlaying = true
                recordAdapter!!.notifyItemChanged(position)
                startPlay(recording.uri, recordItem, view.seekBar, position)
                lastIndex = position
            }
            seekUpdate(view)
        } else {
            if (recording.isPlaying) {
                recording.isPlaying = false
                stopPlaying()
            } else {
                startPlay(recording.uri, recordItem, view.seekBar, position)
                recording.isPlaying = true
                view.seekBar.max = mediaPlayer!!.duration
            }
            recordAdapter!!.notifyItemChanged(position)
            lastIndex = position
        }

        manageSeekBar(view.seekBar)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(record_list)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        getAllRecord()
    }

    private fun getAllRecord() {
        val recordArrayList = ArrayList<Recording>()
        val root = android.os.Environment.getExternalStorageDirectory()
        val path = root.absolutePath + Const.Path
        val directory = File(path)
        val files = directory.listFiles()
        if (files != null) {
            for (i in files.indices) {
                val fileName = files[i].name
                val recordingUri = root.absolutePath + Const.Path + "/" + fileName
                recordArrayList.add(Recording(recordingUri, fileName, false))
            }
            tvNoData.visibility = View.GONE
            recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            recordAdapter = RecordAdapter(recordArrayList)
            recordAdapter!!.setListener(this)
            recyclerView.adapter = recordAdapter
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun seekUpdate(itemView: View) {
        if (mediaPlayer != null) {
            val mCurrentPosition = mediaPlayer!!.currentPosition
            itemView.seekBar.max = mediaPlayer!!.duration
            itemView.seekBar.progress = mCurrentPosition
            lastProgress = mCurrentPosition
        }
        mHandler.postDelayed(Runnable { seekUpdate(itemView) }, 100)
    }

    private fun manageSeekBar(seekBar: SeekBar?) {
        seekBar!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (mediaPlayer != null && fromUser) {
                    mediaPlayer!!.seekTo(progress)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
    }

    private fun startPlay(uri: String?, audio: Recording?, seekBar: SeekBar?, position: Int) {
        mediaPlayer = MediaPlayer()
        try {
            mediaPlayer!!.setDataSource(uri)
            mediaPlayer!!.prepare()
            mediaPlayer!!.start()
        } catch (e: IOException) {
            Log.e(Const.TAG,"failed:" + e.printStackTrace())
        }

        seekBar!!.max = mediaPlayer!!.duration
        isPlaying = true

        mediaPlayer!!.setOnCompletionListener(MediaPlayer.OnCompletionListener {
            audio!!.isPlaying = false
            recordAdapter!!.notifyItemChanged(position)
        })
    }

    private fun markAllPaused(recordingList: ArrayList<Recording>) {
        for (i in recordingList.indices) {
            recordingList[i].isPlaying = false
            recordingList[i] = recordingList[i]
        }
        recordAdapter!!.notifyDataSetChanged()
    }

    private fun stopPlaying() {
        try {
            mediaPlayer!!.release()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mediaPlayer = null
        isPlaying = false
    }

    override fun onBackPressed() {
        super.onBackPressed()
        stopPlaying()
    }
}