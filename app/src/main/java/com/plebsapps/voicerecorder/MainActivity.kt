package com.plebsapps.voicerecorder

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.transition.TransitionManager
import android.util.Log
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

@RequiresApi(api = Build.VERSION_CODES.M)
class MainActivity : AppCompatActivity(), View.OnClickListener {
    private val mHandler = Handler()

    private var fName: String? = null
    private var lastProgress = 0
    private var isPlaying = false
    private var mRecorder: MediaRecorder? = null
    private var mPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getPermissionToRecordAudio()

        ibRecord.setOnClickListener(this)
        ibStop.setOnClickListener(this)
        ivPlay.setOnClickListener(this)
        btRecordList.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.ibRecord -> {
                prepareRecord()
                startRecord()
            }

            R.id.ibStop -> {
                prepareStop()
                stopRecord()
            }

            R.id.ivPlay -> {
                if (!isPlaying && fName != null) {
                    isPlaying = true
                    startPlay()
                } else {
                    isPlaying = false
                    stopPlay()
                }
            }

            R.id.btRecordList -> {
                startActivity(Intent(this, RecordListActivity::class.java))
            }
        }
    }

    private fun prepareStop() {
        TransitionManager.beginDelayedTransition(llRecorder)
        ibRecord.visibility = View.VISIBLE
        ibStop.visibility = View.GONE
        llPlay.visibility = View.VISIBLE
    }

    private fun prepareRecord() {
        TransitionManager.beginDelayedTransition(llRecorder)
        ibRecord.visibility = View.GONE
        ibStop.visibility = View.VISIBLE
        llPlay.visibility = View.GONE
    }

    private fun stopPlay() {
        try {
            mPlayer!!.release()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mPlayer = null

        ivPlay.setImageResource(R.drawable.play)
        timemeter.stop()
    }

    private fun startRecord() {
        mRecorder = MediaRecorder()
        mRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        val root = android.os.Environment.getExternalStorageDirectory()
        val file = File(root.absolutePath + Const.Path)
        if (!file.exists()) {
            file.mkdirs()
        }

        fName = root.absolutePath + Const.Path + "/" + Date().formatToTruncatedDateTime() + ".mp3"

        Log.d(Const.TAG, fName)
        mRecorder!!.setOutputFile(fName)
        mRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)

        try {
            mRecorder!!.prepare()
            mRecorder!!.start()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        lastProgress = 0
        seekBar.progress = 0
        stopPlay()

        timemeter.base = SystemClock.elapsedRealtime()
        timemeter.start()
    }

    private fun stopRecord() {
        try {
            mRecorder!!.stop()
            mRecorder!!.release()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mRecorder = null
        timemeter.stop()
        timemeter.base = SystemClock.elapsedRealtime()
    }

    private fun startPlay() {
        mPlayer = MediaPlayer()
        try {
            mPlayer!!.setDataSource(fName)
            mPlayer!!.prepare()
            mPlayer!!.start()
        } catch (e: IOException) {
            Log.e(Const.TAG, "failed:" + e.printStackTrace())
        }

        ivPlay.setImageResource(R.drawable.pause)

        seekBar.progress = lastProgress
        mPlayer!!.seekTo(lastProgress)
        seekBar.max = mPlayer!!.duration
        seekBarUpdate()
        timemeter.start()

        mPlayer!!.setOnCompletionListener(MediaPlayer.OnCompletionListener {
            ivPlay.setImageResource(R.drawable.play)
            isPlaying = false
            timemeter.stop()
            timemeter.base = SystemClock.elapsedRealtime()
            mPlayer!!.seekTo(0)
        })

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (mPlayer != null && fromUser) {
                    mPlayer!!.seekTo(progress)
                    timemeter.base = SystemClock.elapsedRealtime() - mPlayer!!.currentPosition
                    lastProgress = progress
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
    }

    private var runnable: Runnable = Runnable { seekBarUpdate() }

    private fun seekBarUpdate() {
        if (mPlayer != null) {
            val mCurrentPosition = mPlayer!!.currentPosition
            seekBar.progress = mCurrentPosition
            lastProgress = mCurrentPosition
        }
        mHandler.postDelayed(runnable, 100)
    }

    private fun getPermissionToRecordAudio() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE), Const.RecordAudioRequestCode)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == Const.RecordAudioRequestCode ) {
            if (grantResults.size == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Permission  *NOT*  granted. App is exiting.", Toast.LENGTH_LONG).show()
                finishAffinity()
            }
        }
    }
}