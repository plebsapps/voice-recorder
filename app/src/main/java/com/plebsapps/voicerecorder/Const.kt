package com.plebsapps.voicerecorder

object Const {
    const val TAG = "Voice Record"
    const val RecordAudioRequestCode = 101
    const val Path = "/VoiceRecord"
}